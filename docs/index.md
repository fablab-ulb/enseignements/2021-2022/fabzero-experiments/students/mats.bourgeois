[Site fablab](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/class-website/)


## A propos de moi


<img src="https://cdn.discordapp.com/attachments/715249560527831142/946380426191187998/11146522_10205137565261993_8468806763191826823_n.jpg"
     alt="Markdown Monster icon"
     style="float: left; margin-left: 100px; margin-right: 1000px;height: 150px" />
 


Bonjour, je m'appelle Mats Bourgeois, je suis étudiant en master, en polytechnique option physique, à l'ULB.

Vous pourrez retrouver sur ce site une brève présentation de moi, de mes projets, et de la documentation des modules fablab qui ont été fait.

## Mon background

Comme dit précédemment, j'étudie l'ingénierie en physique, cela fait très longtemps que je m'intéressais à la physique nucléaire, à la mécanique quantique ainsi qu'à l'informatique quantique, le choix de la physique m'était donc évident, j'ai donc choisi de faire des études d'ingénierie, pour faire à terme ce master.

## Projets passés

Je n'ai pas vraiment réalisé de projets avant mon entrée à l'université, mon premier vrai projet est donc le projet interdisciplinaire en Ba1, appelé Projet BaCar, qui consistait en la fabrication d'une petite voiture autonome, qui avait pour but d'avancer sans pilote sur une petite route qui contiendrait différentes indications, comme des feux de signalisations ou des panneaux stop.
Mon deuxième projet était en deuxième année, il fallait réaliser un robot magasinier, qui devait ramener des objets d'un terrain vers une zone de dépôt, en les triant en fonction de leur taille.
J'ai également participé à la réalisation d'un petit jeu en java, qui était une simulation de jeu de vie, un peu comme un sims, où il fallait que le joueur puisse gérer différentes barres de besoin comme la faim, la soif, etc.
Un quatrième projet auquel j'ai participé était un algorithme de recherche de et de comparaison de séquence de protéines dans une base de donnée, en c++.
J'ai aussi codé différents petits projets qui avait surtout pour but de nous initier aux langages ou aux algorithme, notamment:
- Un poker en python
- Une classification de fichier provenant d'une arborescence de fichier en Bash
- Un jeu d'anagramme en réseau en C

Enfin j'ai participé à un projet de génie logiciel, qui consistait en la programmation d'une application en suivant la méthodologie agile de X programming.

### Project A

This is an image from an external site:

![This is the image caption](https://images.unsplash.com/photo-1512436991641-6745cdb1723f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ad25f4eb5444edddb0c5fb252a7f1dce&auto=format&fit=crop&w=900&q=80)

While this is an image from the assets/images folder. Never use absolute paths (starting with /) when linking local images, always relative.

![This is another caption](images/sample-photo.jpg)
