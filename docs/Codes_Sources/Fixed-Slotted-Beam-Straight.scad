$fn=50;
//Parameters

hradius=4;
lradius1=2.5;
lradius2=2.5;
heigh=3;
ecart=8;

length=55;
width=1;

//Figures
translate([0,-length/2,0])
union(){
    translate([-width,0,0])
    cube([2*width,length, heigh]);
    translate([0,-hradius+1,0])
    difference(){
        hull(){
            translate([0,-2*hradius,0]) 
            cylinder(heigh, hradius, hradius);
            cylinder(heigh, hradius, hradius);
        }
        hull(){
            translate([0,-2*hradius,0]) 
            cylinder(heigh, lradius1, lradius1);
            cylinder(heigh, lradius1, lradius1);
        }
    }
    translate([0,length+hradius-1,0])
    difference(){
        hull(){
            cylinder(heigh, hradius, hradius);
            translate([0,hradius+ecart/2,0])
            cylinder(heigh, hradius, hradius);
        }
        union(){
           translate([0,ecart,0])
            cylinder(heigh, lradius2, lradius2);

            cylinder(heigh, lradius2, lradius2);
        }
    }
}