/*
    File : Fixed-Slotted-Beam-Straight
    Author : Mats Bourgeois
    Date : 2022
    License : MIT

*/

$fn = 90;
hradius=4;
lradius1=2.5;
lradius2=2.5;
heigh=3;
ecart=8;

length=55;
width=1;



difference(){
    hull(){
        cylinder(heigh, hradius, hradius);
        translate([0,hradius+ecart/2,0])
        cylinder(heigh, hradius, hradius);
    }
    union(){
       translate([0,ecart,0])
        cylinder(heigh, lradius2, lradius2);

        cylinder(heigh, lradius2, lradius2);
    }
}