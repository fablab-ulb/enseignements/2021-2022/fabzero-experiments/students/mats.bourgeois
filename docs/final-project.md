# Final Project


## Engage - Find a challenge that motivates you

``Big ideas - What is the broad theme or concept I would like to explore ? ``

La surchauffe pendant les canicules

``Essential questions - What are the essential questions that reflect personal interests and the needs of the community ?``



``Challenge - what is your call for action ?``

- Comment développer une méthode frugale de refroidissement d'un corps humain pendant une période de canicule ?

## Investigate - step on the shoulders of the giants not on their toes

``Guiding questions - what are all the questions that needs to be answered in order to tackle the challenge ? Priority ?``

- Quels processus du corps humain permettent déjà un refroidissement partiel du corps ?

- Quelles sont les techiques utilisées par les humains pour se refroidir ?

- Quelles sont les moyens de la nature de se prémunir de la chaleur ?


``Guiding activities/ressources - What resources can I use to answer those questions ? Science.``

- [AskNature](https://asknature.org/?s=cooling&page=0&is_v=1)

- Bibliothèques des Sciences et Techniques de l'ULB

``Analysis and synthesis - write a summary of your findings, facts and data collected``





## Act - Develop a solution, implement it and get feedback

``Solution concepts - What is your solution about ?``

``Solution development - how do this solution solve the challenge ? Prototype and develop.``

``Implementation and evaluation - Experiment and evaluate the solution.``
