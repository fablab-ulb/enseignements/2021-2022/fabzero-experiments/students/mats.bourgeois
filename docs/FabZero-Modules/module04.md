# 4. Impression 3D

Cette semaine portait sur l'impression 3D, on a d'abord eu une introduction sur les domaines d'utilisation de l'impression 3D, puis un rapide tutoriel de l'utilisation de PrusaSlicer. Puis il a été demandé de réaliser ce qu'on appelle un torture test et d'imprimer un ensemble de trois pièces réalisées au Module 2.

## Introduction sur l'impression 3D

L'impression 3D est un domaine qui prend de plus en plus d'importance depuis ces dernières années. Il permet de créer des structures complexes qui sont modélisées au préalable en 3D sur des logiciels comme ceux présentés au Module 2.

Les domaines d'utilisation de ces imprimantes sont variés, passant par exemple par:\
- La mécanique où cela permet de réduire la masse des véhicules.

- L'agroalimentaire, où cela permet d'avoir de meilleures structures, la réduction de déchets ou encore la calorimétrie des aliments

- La santé, où cela permet de faire des prothèses, ou des médicaments dont on contrôle mieux les doses, le taux d'injection, etc. Le tout pouvant être personnalisé pour chaque patient.

## Technique d'impressions
Il y a différentes techniques d'impression qui peuvent être utilisées:

- L'impression sur lit de poudre

- L'impression sur lit de goutte

- Les microseringue pressurisée

- Les dépôts de fondu

- La stéréolithographie

Chacune ayant leurs avantages et leurs inconvénients.

## Installation et réglage de PrusaSlicer

Pour installer PrusaSlicer, je suis allé sur le [site de téléchargement de PruceSlicer](https://help.prusa3d.com/en/downloads/), où j'ai téléchargé les Drivers & Apps de l'"Original PRUSA I3 MK3".

Pour les réglages, j'ai suivi le tutoriel présent sur le [module de cette semaine](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md)

## Exportation du modèle 3D pour l'impression

Une fois le réglage fait, il suffit d'aller sur le plateau, d'appuyer sur découper maintenant pour générer le gcode (type de fichier à envoyer à l'imprimante 3D).

![](./images/Module4/découpeGCODE.jpg)

On vérifie le résultat puis on peut exporter le gcode sur une carte SD que l'on donne à l'imprimante 3D pour la faire travailler.

## Torture Test

**Partie reprise de la [documentation de Sajl Ghani](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/FabZero-Modules/module03/#travail-de-groupe-torture-test)**

Parmi les tâches à faire, un travail de groupe doit être fait et qui consiste à imprimer un torture test. Le site fablab donne un lien qui permet de télécharger le stl d'un [torture test](https://www.thingiverse.com/thing:2806295).

 Les paramètres choisis sont les mêmes que dans [le lien des précautions](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md) avec comme différence, le motif de remplissage est gyroide
, on enlève le support et on diminue les dimensions à 80% (temps d'impression:1H43) pour un temps d'impression plus court (100%=2H50)

et on a obtenu, ce torture test:

Gauche | Face | Droite
:------------:|:------------:|:------------:
![](./images/Module4/test_face.jpg) | ![](./images/Module4/test_droit.jpg) | ![](./images/Module4/test_gauche.jpg)

On peut voir que la plupart des tests ont été réussi par l'imprimante 3D à part le springing test:

![](./images/Module4/stringing.png)

Les petites collonnes sont très fragile. on ne sait pas quand mais ces collonnes se sont décollé du torture test pendant un transfert. Pour rappel, on a redimensionné à 80% la taille d'origine donc on peut faire comme hypothèse qu'avec un épaisseur de colonnes inférieur au diamètre de la buse, un élément peut facilement se casser. Le overhang test est réussi aussi à part que l'arrière est moins lisse.


## Impression des pièces flexibles

Maintenant que le Torture test est réalisé, on peut passé à la partie de l'impression. Il est demandé de faire un kit de trois pièces imprimées. Pour ce faire, j'ai repris mon code de la pièce du [Module 2](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/mats.bourgeois/FabZero-Modules/module02/). Pour les deux autres, je reprends des codes déjà existants dans les modules d'autres personnes que vous pouvez retrouver sur ces liens :

- [Le "Satellite" de Doriane Galbez du 20/02/2021](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/FabZero-Modules/module02/#piece-7-satellite)

- [Le "Double Z spring" de Lisart Théo datant de 2021](https://gitlab.com/theo.lisart/parametric_flexlinks)

Pour le choix des dimensions, j'ai pris le schéma suivant d'une pièce de Lego, qui contient les données dont j'ai besoin :

![](./images/Module4/LegoSize.svg.png)

J'ai ensuite décidé d'essayer une première impression, afin d'avoir une première idée. En voici le résultat :

![](./images/Module4/PremiereImpression.jpg)

Comme on pouvait s'y attendre, la pièce ne s'emboite pas parfaitement avec un lego, car il y a une certaine erreur sur l'impression de la pièce. Comme on peut voir sur l'image suivante :

![](./images/Module4/EncastrementImparfait.jpg)

Cette pièce m'aura également permi de réaliser que la structure est fragile, comme en témoigne cette image plus récente de la pièce :

![](./images/Module4/restes_premiere_piece.jpg)

Afin d'avoir une meilleure idée des rayons à choisir, j'ai décidé de modéliser une pièce pour représenter les mesures critiques de la pièce, en voici le code source :

```
/*
    File : Calibration_Piece_Flexible
    Author : Mats Bourgeois
    Date : 2022
    License : MIT

*/

$fn = 90;
hradius=4;
lradius1=2.5;
lradius2=2.5;
heigh=3;
ecart=8;

length=55;
width=1;



difference(){
    hull(){
        cylinder(heigh, hradius, hradius);
        translate([0,hradius+ecart/2,0])
        cylinder(heigh, hradius, hradius);
    }
    union(){
       translate([0,ecart,0])
        cylinder(heigh, lradius2, lradius2);

        cylinder(heigh, lradius2, lradius2);
    }
}
```

J'ai ensuite imprimé cette pièce avec des rayons différents jusqu'à avoir l'emboitement souhaité. Le rayon de 2.5 mm semblait être le choix le plus adapté.

![](./images/Module4/calibration.jpg)

Maintenant que le rayon est décidé, il m'a suffit de modifier les paramètres des pièces à imprimer, et enfin me restait l'impression à faire. Le tableau suivant reprend les pièces imprimées.

Fixed Slotted Beam | Double Z | Satellite
:------------:|:------------:|:------------:
![](./images/Module4/Fixed.jpg) | ![](./images/Module4/DoubleZ.jpg) | ![](./images/Module4/Satellite.jpg)

Ne reste plus que le kit de 3 pièces qu'il était demandé de réaliser, voici donc une petite catapulte (bon, j'avoue ne pas avoir été très inspiré)

![](./images/Module4/Catapulte.jpg)



Et voici la video du fonctionnement :

![type:video](./images/Module4/Video_Catapulte_Compresse.webm)


Et dans le cas où celle ni ne fonctionnerait pas : [lien](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/mats.bourgeois/-/blob/main/docs/FabZero-Modules/images/Module4/Video_Catapulte_Compresse.mp4)
