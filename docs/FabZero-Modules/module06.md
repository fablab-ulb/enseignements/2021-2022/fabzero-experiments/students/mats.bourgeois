# 6. Electronique 2 - Fabrication

Le module de cette semaine concernait la fabrication d'un microprocesseur.

## Introduction microprocesseur

Le but est de créer un circuit du genre de l'arduino mais en plus avancé. Le chip sera donc bien plus complexe mais l'arduino peut gérer cette complexité.

Cependant, contrairement aux arduino classiques, les actions qui paraissent simple vont avoir une couche bien plus lourde pour traduire les actions (par exemple faire clignotter une led utilise beaucoup de mémoire).

La communication entre un processeur et l'ordinateur pour induire du code se fait classiquement par un port particulier, avec un code qui se lance automatiquement pour assurer cette communication, appelé le bootloader.

Dans le processeur qui sera fabriqué dans ce module, la communication se fait directement par usb, l'avantage est que du coup il ne faut que l'usb pour communiquer avec l'ordinateur, en revanche, le bootloader prendra bien plus de mémoire que pour une communication classique.

## Processus de création de microprocesseur

Pour réaliser un microprocesseur, il faut tout d'abord le pcb sur lequel on va souder les pièces. Les PCB sont fournis directement et il a suffit de réaliser les soudures.

## Pièces du microprocesseur

Les pièces à souder sur le PCB sont listées ci-dessous :

- Le chip

- Un régulateur de tension car le chip prend un maximum de 3.3V (sinon il grille), alors que l'usb fournit une tension de 5V.

- Deux capacitors

- Deux led (une rouge et une verte) et resistances de 3,3kOhm

- Une résistance de 1001 Ohm

- Deux groupe de 6 pins

## Apprentissage de la soudure

Voici le kit de soudure :

![](./images/Module6/MaterielSoudure.jpg)

Pour souder, on utilise (bien sûr) un fer à souder :

![](./images/Module6/FerASouder.jpg)

L'éponge sur cette image sert à frotter le fer pour retirer l'oxydation qui se dépose dessus pendant la soudure, qui gêne car elle empêche la soudure de se déposer correctement sur le fer.

Ce fer à souder est équipé d'une régulateur de température, qui donne la température du fer à souder à tout moment. La température souhaitée est entre 300°C et 350°C max, plus ce sera chaud plus vite ça ira, cependant le chip n'étant pas fait pour être soumis à de grandes températures, on limite la température de la soudure.

![](./images/Module6/RegulateurTemperature.jpg)

Celui-ci fera fondre le bout d'un fil d'étain, que l'on dépose sur le cuivre du PCB, sur lequel il refroidira rapidement, créant une soudure. L'effet de capilarité sur les branches métalliques des pièces à souder facilite la soudure de celles-ci.

Voici une image du fil d'étain :

![](./images/Module6/FilSoudure.jpg)

Pour manipuler les pièces à souder en évitant de se brûler, on utilise une pince comme celle-ci :

![](./images/Module6/Pince.jpg)

Pour réaliser une bonne soudure, il faut prendre la goutte sur la surface plate au bout du fer à souder, en faisant attention à ne pas en prendre une trop grande, sinon on fait de grosses billes !

Ces billes peut être retiré à l'aide d'une pompe, comme celle-ci

![](./images/Module6/Pompe.jpg)

Pour s'en servir, on arme le piston, on fait chauffer la partie à enlever avec le fer à souder, et on appuie sur le bouton avec le bout de la pompe à côté de la partie à enlever. Il ne faut surtout pas hésiter à ajouter de la soudure pour se faciliter la tâche.

![](./images/Module6/PompeArmee.jpg)

## Etapes de la soudure

Maintenant que j'ai introduit les méthodes de soudures, on peut passer à la pratique.

### Description PCB et des étapes

Voici le PCB avant la soudure

![](./images/Module6/PCBVide.jpg)

Afin de se faciliter la tâche, j'ai réalisé la soudure dans l'ordre suivant : le chip, le convertisseur, le capacitor C2, la résistance de 1001 Ohm, les résistances de 3300 Ohm et leur diode, et enfin les pins et enfin le capacitors.

### Soudure du chip

Pour se faciliter la tâche, on commence par poser la soudure dans un des coins de l'endroit où le chip devra être placé.

Ensuite, on fait fondre cette soudure et on pose le chip dessus.

![](./images/Module6/PremiereSoudure.jpg)

Une fois que c'est fait, on soude le coin opposé de la pin, afin de pouvoir la fixer.

Une fois ceci fait, il suffit en théorie de souder chaque pin une à une... c'est plus compliqué que ça et on peut voir que j'ai eu un peu de mal sur cette image :

![](./images/Module6/Soudure_Bug.jpg)

Mais tout est bien qui finit bien, après plusieurs utilisation de la pompe, des efforts de soudure, voici le résultat (bizarrement sale à cause d'un liquide plutôt visqueux mais je n'ai jamais compris ce que c'était)

![](./images/Module6/ChipSoude.jpg)

### Soudure du régulateur

Une fois que le chip est soudé, on peut passer au régulateur. J'ai commencé par faire les petites branche (d'abord une soudure, puis poser la pièce, et enfin faire les soudures restantes). Attention : Si vous ne faites pas attention, vous poserez de la soudure sur le chip et il faudra revenir un peu en arrière pour corriger ça.

![](./images/Module6/RegulateurSoude.jpg)

### Soudure du reste

Une fois que ces pièces sont soudées, c'est rebelotte, on soude toutes les pièces, en commençant toujours par déposer de la soudure sur une branche, puis poser la pièce en faisant fondre la soudure posée, et enfin souder le reste. L'important est d'être méthodologique !

Note importante, faire attention au sens de la LED !

Voici la pièce, une fois tous les éléments soudés dessus.

![](./images/Module6/FinSoudure.jpg)

## Configuration du microprocesseur

Une fois que la soudure est finie, il faut installer le bootloader sur le processeur.

On voit que le microprocesseur fonctionne car il apparait dans la liste des périphérique de l'ordinateur sur lequel il est branché.

![](./images/Module6/Peripherique.jpg)

On peut également observé que la LED verte est allumée lorsque celui-ci fonctionne correctement

![](./images/Module6/ProcesseurBranche.jpg)

## Utilisation du microprocesseur

Maintenant que le microprocesseur est fabriqué et configuré, on peut l'essayer !

### Configuration d'arduino

Tout d'abord, comme le chip n'est pas un arduino officiel, il faut configurer le configurer, on peut simplement suivre le tutoriel disponible sur ce [lien du module](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero/electronics/-/blob/master/Make-Your-Own-Arduino.md#settings), qui reprend également la liste des pins du circuit.

### Blink de la led

Le microprocesseur étant fonctionnel, on peut désormais simplement essayer de lui faire appliquer un programme simple, à savoir l'exemple de blink de l'arduino, en changeant la led à faire briller, en effet, c'est la pin 15 qui est reliée à la LED, comme indiqué sur le [tableau](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero/electronics/-/blob/master/Make-Your-Own-Arduino.md#pinout) précédemment cité.

Voici le code source :

```
/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman
  modified 2022
  by Mats Bourgeois

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink
*/

// the setup function runs once when you press reset or power the board

int led= 15;
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(led, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

Et voici la LED rouge en train de briller :

![](./images/Module6/LEDRougeAllumee.jpg)

Et en train de clignotter... si la vidéo fonctionne :

![type:video](./images/Module6/BlinkVideo_Compressee.webm)

Dans le cas où la vidéo ne fonctionnerait pas, voici le [lien vers la vidéo dans les fichiers Git](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/mats.bourgeois/-/blob/main/docs/FabZero-Modules/images/Module6/BlinkVideo_Compressee.mp4)
