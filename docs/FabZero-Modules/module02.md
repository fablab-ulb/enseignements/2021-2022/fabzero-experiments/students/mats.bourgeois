# 2. Conception Assistée par Ordinateur

Cette semaine portait sur la conception assistée par ordinateur, et plus spécifiquement sur le modélisation 3D sur des logiciel open source.

## 1 Pourquoi faire de la modélisation ?

La modélisation permet de faire du prototypage sur ordinateur, cela économise du tmeps et de l'argent qui aurait été normalement mis sur les premiers prototypes, mais également le remaniement plus simple de ceux-ci, on peut en effet modifier des paramètres rapidement, comme la longueur d'une pièce, etc. Ce qui n'aurait pas été si rapide à faire sur un prototype réel.

## 2 Quels logiciel utiliser ?

Il existe deux types de logiciel, les logiciels propriétaires (ex: Solidworks), et les logiciels open source (ex: FreeCAD). Ceux-ci ont chacun des avantages et défaut, en effet là où les logiciel propriétaires ont peut être une prise en main plus facile ou plus de fonction qu'un logiciel open source, ils ont le défaut d'être souvent très onéreux, et on dépend également de l'état financier du propriétaire, si celui-ci fait faillite, on perd potentiellement toute possibilité de récupérer son projet. On comprend donc l'intérêt d'apprendre à utiliser des logiciels open source.

Voici donc ma documentation sur l'utilisation d'OpenSCAD

### OpenSCAD

OpenSCAD est un logiciel permettant de réaliser de la modélisation 3D à l'aide de lignes de code, grâce à des fonction prédéfinies que l'on peut retrouver sur [la cheatsheet](https://openscad.org/cheatsheet/). En voici un guide d'utilisation des fonctions de base.

#### Apprentissage des fonctions utiles

Afin de réaliser la pièce demandée pour cette semaine, il était nécessaire d'apprendre à utiliser les fonction d'OpenSCAD, notamment les cylindre, les cubes ainsi que les fonction permettant de fusionner les formes. Voici donc les fonctions utiles à la réalisation de la pièce demandée:

##### Cube

```
cube([largeur, profondeur, hauteur], center)
```

Permet de créer un parallélépipiède rectangle (que j'appellerai cube) de dimensions **largeur**, **profondeur**, **hauteur** (en mm), le paramètre **center** est facultatif et sert à indiquer via "center = true", que l'on souhaite placer le centre du cube à l'origine.

Exemple :

Code  | Figure
:-----------------:|:--------------------------------------------------:
cube([5,8,2]); |  ![](./images/Module2/Exemple_Cube.jpg)


Si l'objet à créer est un simple cube, il est possible de simplement écrire

```
cube(taille, center)
```

Où **taille** sera simplement la longueur d'une arête.

##### Cylinder

```
cylinder(hauteur, rayon_inférieur, rayon_supérieur)
```

Cette commande crée un cylindre à base circulaire, de hauteur **hauteur** (en mm), avec une base inférieur de rayon **rayon_inférieur**, et avec une base supérieure de rayon **rayon_supérieur**. On peut également y ajouter le paramètre center, tout comme pour le cube. Par défaut, le centre de la base inférieure du cylindre est placée à l'origine et l'objet est créé au dessus.

Exemple :

Code  | Figure
:-----------------:|:--------------------------------------------------:
cylinder(5,10,5, center = true); |  ![](./images/Module2/Exemple_Cylindre.jpg)

On peut voir que le cylindre n'est pas très lisse. Pour changer cela, on introduit un paramètre **$fn** qui permet de lisser les surface arrondie. Par exemple, ceci est la même figure que l'exemple, mais avec un **$fn=100** :

![](./images/Module2/Exemple_FN100.jpg)

##### Union, Difference, Translate

Ces fonctions ont pour point commun d'influencer les formes de base :

- Union sert à lier deux formes

- Difference sert à faire un trou d'une certaine forme dans une autre

Exemple :

```
difference(){
    cube(5,center=true);
    cube([2,2,5], center=true);
}
```

donnera un cube troué :

![](./images/Module2/Exemple_Difference.jpg)

- Translate sert à déplacer l'origine à laquelle sera posée l'objet.

##### Hull et minkowski

Ces fonctions servent à "fusionner" des objets pour créer de nouvelles figures.

- Hull sert typiquement à lier deux cylindre pour faire un cylindre à base ellipsoïdal

Exemple :

```
hull(){
    cylinder(5,10,10);
    translate([10,0,0])
    cylinder(5,10,10);
}
```

Crée la figure :

![](./images/Module2/Exemple_Hull.jpg)

- Minkowski peut typiquement servir à lier un cube à des cylindre pour créer un cube à coin arrondis

Exemple :

```
minkowski(){
    cylinder(5,10,10);
    translate([10,0,0])
    cube(10);
}
```

Sans Minkowski  | Avec Minkowski
:-----------------:|:--------------------------------------------------:
 ![](./images/Module2/Exemple_Mink_Disabled.jpg) |  ![](./images/Module2/Exemple_Mink_Enabled.jpg)

### Modélisation paramétrique

Afin de rendre le modèle plus efficace, il est important de le réaliser de manière paramétrique, c'est à dire que l'on décrit des paramètres en amont que l'on utilise dans le code en lui même. Cela facilite les modification de ces paramètres.

## 3 Modélisation d'une pièce flexible

Voici la pièce que j'ai décidé de modéliser:

Il y a trois parties importantes dans cette pièce:

- La tige

- L'anneau avec un creux ellipsoïdal

- L'anneau doublement creusé


La partie la plus facile à modéliser est bien sûr la tige, qui peut être modélisée par un simple cube.

```
cube([2*width,length, heigh]);
```

Pour modéliser les anneaux, j'ai décidé d'opter pour la fonction la plus directe, la fonction hull, qui permet d'obtenir directement le cylindre à base ellipsoïdal que je souhaite. Il suffit ensuite d'utiliser la fonction difference entre ce cylindre et un autre cylindre de rayon inférieur (translaté à la bonne position), afin d'obtenir l'anneau à creux ellipsoïdal.

```
difference(){
    hull(){
        translate([0,-2*hradius,0])
        cylinder(heigh, hradius, hradius);
        cylinder(heigh, hradius, hradius);
    }
    hull(){
        translate([0,-2*hradius,0])
        cylinder(heigh, lradius1, lradius1);
        cylinder(heigh, lradius1, lradius1);
    }
}
```

Pour l'anneau doublement creusé, on peut repartir du cylindre ellipsoïdalet le creuser, de nouveau avec la fonction difference, avec deux cylindre à base sphérique, séparé d'une certaine distance.

```
difference(){
    hull(){
        cylinder(heigh, hradius, hradius);
        translate([0,hradius+ecart/2,0])
        cylinder(heigh, hradius, hradius);
    }
    union(){
       translate([0,ecart,0])
        cylinder(heigh, lradius2, lradius2);

        cylinder(heigh, lradius2, lradius2);
    }
}
```

On utilise la fonction translate pour décaler les trois parties, et on applique la fonction fusion sur les trois, pour finalement obtenir la pièce souhaitée. Le code source est repris ci-dessous. La license ci-dessous indique comment l'objet peut être publié.


```
/*
    File : Fixed-Slotted-Beam-Straight
    Author : Mats Bourgeois
    Date : 10-03-22
    License : MIT

*/


$fn=50;
//Parameters

hradius=4;
lradius1=2.5;
lradius2=2.5;
heigh=3;
ecart=8;

length=55;
width=1;

//Figures
translate([0,-length/2,0])
union(){

    //TIGE

    translate([-width,0,0])
    cube([2*width,length, heigh]);

    //ANNEAU SIMPLEMENT TROUE

    translate([0,-hradius+1,0])
    difference(){
        hull(){
            translate([0,-2*hradius,0])
            cylinder(heigh, hradius, hradius);
            cylinder(heigh, hradius, hradius);
        }
        hull(){
            translate([0,-2*hradius,0])
            cylinder(heigh, lradius1, lradius1);
            cylinder(heigh, lradius1, lradius1);
        }
    }

    // ANNEAU DOUBLEMENT TROUE


    translate([0,length+hradius-1,0])
    difference(){
        hull(){
            cylinder(heigh, hradius, hradius);
            translate([0,hradius+ecart/2,0])
            cylinder(heigh, hradius, hradius);
        }
        union(){
           translate([0,ecart,0])
            cylinder(heigh, lradius2, lradius2);

            cylinder(heigh, lradius2, lradius2);
        }
    }
}
```

Et voici le résultat :

![](./images/Module2/Fixed_Slotted_Beam_Straight.jpg)
