# 5. Electronique 1 - Prototypage

Cette semaine portait sur l'utilisation d'arduino. Un arduino est un microprocesseur, que l'on peut contrôler avec du code, qui est écrit en langage arduino, qui est dérivé du C.

## Schéma de l'arduino Uno

Le schéma suivant reprend la structure de l'arduino Uno.

![](./images/Module5/Arduino-uno-pinout.png)

Ce schéma reprend la liste des pins, c'est à dire la liste des input et output du controller.

## Apprentissage de l'arduino

### Envoyer un code sur l'arduino

La première chose que l'on peut faire pour apprendre l'arduino est de lire les codes de l'exemple. On remarque que la structure est toujours la même : on initialise d'abord les ports auxquels on va connecter les pins, puis on a une loop infinie dans laquelle on écrit le code exécuté par l'arduino.

Pour commencer, on peut essayer les codes exemples présents de base sur arduino.

![](./images/Module5/ExempleArduino.png)

On peut commencer par utiliser le code blink, dont voici le code :

```
/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman
  modified 2022

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink
*/

// the setup function runs once when you press reset or power the board

int led= 15;
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(led, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

Pour l'utiliser sur l'arduino, il suffit de brancher l'arduino à l'usb de l'ordinateur, de sélectionner le bon port dans la section "Outils" de du logiciel arduino :

![](./images/Module5/PortSettings.jpg)

Enfin, une fois que tout ceci est fait, il suffit d'appuyer sur téléverser :

![](./images/Module5/Televerser.jpg)

Et la LED présente sur l'arduino Uno devrait commencer à clignotter.

IMAGE SI POSSIBLE

### Premier code : Bouton

La première chose que j'ai décidé de réaliser sur l'arduino est la fabrication d'un bouton, qui une fois appuyé allume ou éteint une LED.

Pour ce faire, j'ai d'abord dû faire le circuit, repris sur ce schéma provenant du [site d'arduino](https://www.arduino.cc/en/Tutorial/BuiltInExamples/Button) :

![](./images/Module5/Circuit_Bouton.png)

Ce qui donne en pratique :

Board | Arduino
:--------:|:--------:
![](./images/Module5/BoardBouton.jpg) | ![](./images/Module5/Arduino_Bouton.jpg)

Pour reprendre le circuit ci-dessus, nous avons un bouton, relié à la *Terre* et au *5V* d'un côté, et au **pin 8** de l'autre.

Nous avons également une simple LED connectée à la *Terre* et à la **pin 9**.

Ces deux circuits présentent également une résistance, ce qui est important pour éviter de brûler les composants.

Voici une première version du code pour faire un bouton fonctionnel :

```c
/*
    File : Bouton1
    Author : Mats Bourgeois
    Date : 2022
    License : MIT

*/

int pushButton = 8;
int led=9;
int clicked=0;
int state=0;
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(led, OUTPUT);
  Serial.begin(9600);
  pinMode(pushButton, INPUT);
}

// the loop function runs over and over again forever
void loop() {
  // set the brightness of pin 9:
  clicked= digitalRead(pushButton);
  Serial.println(clicked);
  // change the brightness for next time through the loop:
  if (clicked){
    state++;
    if(state==1){
      digitalWrite(led, HIGH);
    }
    else{
      state=0;
      digitalWrite(led, LOW);
    }
  }
  delay(1);
}
```

Le principal problème de ce code est qu'une fois que l'on appuie sur le bouton, la LED va clignotter plusieurs fois, car le bouton est un système mécanique qui va rebondir.

Voici donc une deuxième version du code, où cette fois je rajoute du délai entre le moment où le bouton est cliqué et le moment où le processeur reprend sa lecture.

```c
/*
    File : Bouton1
    Author : Mats Bourgeois
    Date : 2022
    License : MIT

*/

int pushButton = 8;
int led=9;
int stopLED=7;
int clicked=0;
int state=0;
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(led, OUTPUT);
  pinMode(stopLED, OUTPUT);
  Serial.begin(9600);
  pinMode(pushButton, INPUT);
}

// the loop function runs over and over again forever
void loop() {

  clicked= digitalRead(pushButton);
  digitalWrite(stopLED,LOW);
  Serial.println(clicked);


  if (clicked){
    digitalWrite(stopLED,HIGH);
    state++;
    if(state==1){
      digitalWrite(led, HIGH);
    }
    else{
      state=0;
      digitalWrite(led, LOW);
    }
    delay(500);
  }
  delay(1);
}
```

J'ai également ajouté une LED Rouge à la pin 7 afin d'indiquer l'état du circuit (c'est à dire si appuyer sur le bouton change quelque chose ou non.)

Il y a donc maintenant un délai d'une demi-seconde entre deux appuis possible du bouton.

Voici tout d'abord le nouveau circuit :

![](./images/Module5/Arduino_Bouton2.jpg)

Et une démonstration du fonctionnement :

![type:video](./images/Module5/Demo_Bouton_2_Compresse.webm)

### Mesure d'une grandeur physique

Maintenant que l'on a appris à se servir un peu d'arduino, essayons de mesurer une grandeur réelle et d'en faire quelque chose.

Pour ce faire, je vais utiliser un capteur de température (le LM 35) dont on peut trouver des informations sur ce [lien](https://www.makerguides.com/lm35-arduino-tutorial/)

Voici le schéma du circuit pour faire cette mesure :

![](./images/Module5/Temperature_Sensor_Circuit.webp)

En pratique, cela donne ceci (A noter que la pin A0 est un input analogique, ce qui signifie qu'il mesure une tension analogique, qu'il convertit en une valeur numérique) :

Board | Arduino
:--------:|:--------:
![](./images/Module5/Board_Temperature.jpg) | ![](./images/Module5/Arduino_Temperature.jpg)

Maintenant, on peut simplement utiliser l'exemple d'AnalogReadSerial présent dans l'arduino de base :

```c
/*
  AnalogReadSerial

  Reads an analog input on pin 0, prints the result to the Serial Monitor.
  Graphical representation is available using Serial Plotter (Tools > Serial Plotter menu).
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/AnalogReadSerial
*/

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  // print out the value you read:
  Serial.println(sensorValue);
  delay(1);        // delay in between reads for stability
}
```

Et pour lire les valeurs, on va dans les **Outils** et on ouvre le Traceur Série (on peut également utiliser le moniteur série).

Temperature Ambiante | Temperature doigt posé
:--------:|:--------:
![](./images/Module5/Temp_Ambiante.jpg) | ![](./images/Module5/Temperature_DoigtPose.jpg)

On voit que dans la pièce dans laquelle je suis, la température est convertie en un nombre, environ 36, et quand je pose mon doigt sur le moniteur, la température augmente jusqu'aux environ de 52. Mais que signifient ces valeurs en température réelle ? Pour ceci, on cherche la courbe Tension-Température, que j'ai trouvée sur ce [lien](https://www.makerguides.com/lm35-arduino-tutorial/)

![](./images/Module5/Courbe_Temp_Tension.png)

Ce lien contient également un programme intéressant qui permet à l'arduino de directement retourner la température au Moniteur Série. Voici le code :

**Rappel : La source se trouve [ICI](https://www.makerguides.com/lm35-arduino-tutorial/)**

```c
/* LM35 analog temperature sensor with Arduino example code. More info: https://www.makerguides.com */

// Define to which pin of the Arduino the output of the LM35 is connected:
#define sensorPin A0

void setup() {
  // Begin serial communication at a baud rate of 9600:
  Serial.begin(9600);
}

void loop() {
  // Get a reading from the temperature sensor:
  int reading = analogRead(sensorPin);

  // Convert the reading into voltage:
  float voltage = reading * (5000 / 1024.0);

  // Convert the voltage into the temperature in degree Celsius:
  float temperature = voltage / 10;

  // Print the temperature in the Serial Monitor:
  Serial.print(temperature);
  Serial.print(" \xC2\xB0"); // shows degree symbol
  Serial.println("C");

  delay(1000); // wait a second between readings
}
```

Voici mes nouvelles mesures, cette fois dans le moniteur série :

Temperature Ambiante | Temperature doigt posé
:--------:|:--------:
![](./images/Module5/Temperature_Ambiante_DegreC.jpg) | ![](./images/Module5/Temperature_Doigt_Pose_DegreC.jpg)

En utilisant ce code, je peux donc directement lire la température. La température ambiante est donc de 17.6°C, et lorsque que je pose le doigt, elle monte presque à 26°C. On a donc mesuré une température à l'aide du capteur de température.
