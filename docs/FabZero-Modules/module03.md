# 3. Découpe assistée par ordinateur

## 1. Découpe laser en deux mots

Comme son nom l'indique, la découpe laser utilise la puissance d'un laser pour couper de la matière, mais cela permet également de faire des gravures. Il faut cependant faire attention à certains points. Tout d'abord, il y a les précaution d'usage, puis il faut vérifier que le matériau que l'on veut découper ne soit pas interdits (ou déconseillés).

## 2. Précaution d'usage

Lors de l'utilisation d'une découpeuse laser, il est important de respecter les règles de sécurité suivantes reprises sur ce [lien](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md#pour-%C3%A9viter-tout-risque-dincendie).

Il faut également réfléchir au matériau que l'on utilise, en effet, certains ne sont pas appropriés à la découpe, généralement à cause de leur production de vapeur toxique, ou encore au fait qu'ils réfléchissent la lumière, et donc le laser. Les matériaux à utiliser ou non sont repris sur ce [lien](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md#mat%C3%A9riaux-pour-la-d%C3%A9coupe).

## 3. Découpeuses utilisées

Les découpeuses présentes dans le fablab sont les suivantes :

- L'[epilog fusion pro 32](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md#epilog-fusion-pro-32)

- La [Lasersaur](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md#lasersaur)

- La [Full Spectrum Muse](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md#full-spectrum-muse)

## Travail de groupe

Le travail de groupe consistait en l'apprentissage d'utilisation d'une des découpeuses précédemment citées. Mon groupe a travaillé sur la lasersaur.

Pour utiliser cette découpeuse, il faut d'abord activer la pompe à air comprimé, le refroidissement, ainsi que le filtre, servant respectivement à :

- La pompe à air comprimé sert à souffler de l'air sur la pièce à découper pour éviter qu'elle ne prenne feu.

![](./images/Module3/AirComprime.jpg)

- Le refroidissement sert évidemment à éviter une surchauffe de la machin

![](./images/Module3/Refroidisseur.jpg)

- Le filtre sert à filtrer les éléments soufflés par la pompe

![](./images/Module3/Filtre.jpg)

Une fois que ces éléments sont activés, on doit régler la focalisation du laser. Ceci se fait avec cet élément :

![](./images/Module3/Piece_Focal.jpg)

Placée comme ceci :

![](./images/Module3/Focal.jpg)

Maintenant que la focalisation est faite, on peut essayer de découper une pièce. Pour ce faire, on va commencer par la découpe et la gravure d'une pièce simple : une ellipse.

Pour ce faire, on a choisi un éléments dont on avait déjà une certaine idée de la calibration, qu'on a ensuite découpé et gravé avec des paramètres très clair (c'est à dire qu'on a pris des paramètres qui ne coupaient clairement pas pour la gravure et des paramètres qui coupaient forcément pour la découpe).

![](./images/Module3/Ellipse.jpg)

On a ensuite choisir l'ordre de découpe. Celui à choisir doit de préférence être le suivant : D'abord les éléments à graver (en rouge sur l'image précédente), puis les éléments à découper (en noir), pour éviter que la découpe ne perturbe le matériau à graver.

![](./images/Module3/EllipseDecoupee.jpg)

Maintenant que l'ellipse est découpée. On va maintenant réaliser une calibration, c'est à dire qu'on va regarder la profondeur de la découpe en fonction des paramètres choisis.

On reprend la feuille du matériau choisi. Pour nous faciliter la tâche, le dessin pour la calibration est déjà présent dans le rasberry pi lié à la lasersaur, donc il a suffit de choisir les paramètres de découpe.

La calibration donne le résultat suivant :

![](./images/Module3/Calibration.jpg)

Qui est facile à lire : les paramètres où le résultat donnent un trou servent à réaliser des découpes, tandis que les paramètres où il reste du matériau serviront plutôt à la gravure. On peut rapidement voir que si l'on a une idée de la résistance du matériau, on peut négliger la partie en bas à gauche de la calibration.

## Création d'un Kirigami

Une application possible de la découpe assistée par ordinateur est la création de kirigamis, qui sont des objets fabriqués par découpes et pliages successif d'une feuille de matière première (par exemple du papier). Pour ce faire, il suffit de faire une modélisation des découpes à faire sur un logiciel de dessins (exemple: inkscape). Les découpes servent à faire les découpes du kirigami (comme on peut s'en douter) tandis que les gravures serviront à faciliter les pli.\

Voici comment réaliser une enveloppe en kirigami. Mais avant celà, il était nécessaire d'apprendre à utiliser inkscape. Voici donc un suivi de ce qui a été réalisé dans le but de créer une enveloppe.\

### Utilisation d'Inkscape

Pour commencer, il me fallait savoir comment créer des formes simples : des rectangles et des triangles, de préférence en segments distincts car il faut que les lignes à découper soit d'une couleur différentes des lignes à graver.


Pour commencer, j'ai créé un rectangle, que j'ai décidé de dupliquer afin de fournir une base à la création d'un triangle.

![](./images/Module3/Enveloppe1.jpg)

Une fois ce triangle réalisé, j'ai décidé de passer à la dissociation du rectangle en 4 segments. La solution la plus simple que j'aie trouvée était de tracer un segment par côté du rectangle, avant de cacher ce dernier. Une fois cela fait, j'ai réalisé que cela n'était pas nécessaire car aucun des côtés du rectangle n'était à couper, ce faisant il me suffisait de mettre une couleur distincte de celle du triangle.

Pour les trois faces restantes, on peut prendre deux triangles et un trapèze, les deux triangles devant être symétriques, il suffit d'en créer un, de la même manière que le premier a été réalisé, puis de le dupliquer et d'appliquer une symétrie.

![](./images/Module3/Enveloppe2.jpg)

Ne reste plus que le trapèze, qui peut être créé en utilisant un segment partant d'un coin inférieur droit, qu'on dupplique avec une symétrie que l'on colle au coin inférieur gauche (cf figure ci-dessous). Enfin on relie ces deux segments par une troisième, ce qui forme le trapèze.

![](./images/Module3/Enveloppe3.jpg)

Pour vérifier que la lettre a une bonne géométrie, on peut faire glisser les faces adjacente dans le rectangle central pour adapter la taille.

![](./images/Module3/Enveloppe4.jpg)

Une fois cela réalisé, on peut jouer sur la courbure des figures adjacente au rectangle pour obtenir une lettre plus stylisée, en faisant attention à ce que les figures collent toujours entre elles. Pour ce faire, j'utilise le traceur de courbes de bézier, en suivant simplement les points de ma lettre initiale.\
![](./images/Module3/Enveloppe5.jpg)

Et voici le résultat, après ajout d'un logo d'une marque que j'apprécie pour lui donner un petit style :

![](./images/Module3/EnveloppeFinal.svg)

### Découpe au laser

Maintenant que le modèle a été réalisé, il faut choisir une découpeuse laser, j'ai décidé d'opter pour l'epilog fusion pro 32. J'ai choisi cette découpeuse car on m'en a dit que l'utilisation était simple et qu'elle découpait rapidement.

Pour utiliser cette découpeuse, j'ai suivi le [tutoriel de ce groupe](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module03/#35-group-assignment-epilog-fusion-pro-32).

Note : Au début je n'ai pas compris comment utiliser le filtre. La découpeuse dégageait une odeur de brûlé. Si vous sentez le brûlé, vérifiez donc que cet élément est bien allumé.

![](./images/Module3/Cooler2.jpg)

Passons maintenant à la découpe. J'ai choisi une feuille de papier cartonnée dont une calibration pour une feuille similaire était disponible. J'ai donc décidé de reprendre cette calibration telle quelle... Et ce n'était malheuresement pas très efficace car j'ai dû refaire plusieurs impression pour enfin obtenir ma pièce. Gaspillant donc une feuille entière car je n'ai pas réalisé de calibration.

Voici mes paramètres de découpe finaux :

![](./images/Module3/Parametres.jpg)

Pour être plus efficace, j'aurais dû réalisé une calibration sur les paramètres critiques de ma pièce, à savoir : gravure des courbes à découper, et "découpe" faible des lignes. A noter que les paramètres de découpe pour le mode "gravure" et pour le mode "ligne droite" sont très différents.

J'ai tout de même finalement obtenu comme résultat cette belle enveloppe.. que j'ai malheureusement un peu déchiré au début.

![](./images/Module3/ApresDecoupe.jpg)

 Mais une fois collée, on voit une belle enveloppe avec des courbes qui auraient été difficile à réaliser sans fabrication assistée par ordinateur.

 ![](./images/Module3/EnveloppeCollee.jpg)

 ![](./images/Module3/EnveloppeFermee.jpg)
