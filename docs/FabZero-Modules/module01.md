# 1. Documentation

Le premier module de ce cours concerne la documentation et l'utilisation du version control via l'utilisation de git. Mais avant cela, il est bon de rappeler l'utilité de ces deux points.

## Pourquoi faire de la documentation ?

La documentation est importante pour plusieurs raisons.

Tout d'abord, elle permet aux personnes étant intéressées par la réalisation d'un projet d'en comprendre sa méthodologie, ceci implique qu'il est nécessaire de ne pas seulement inclure les réussites mais également les erreurs, les choses qui n'ont pas fonctionnées, car ces points sont importants pour être conscient de ce qui a été essayé, comment ces choses ont été testées, mais également pourquoi elles ont été abandonnées.

Un autre avantage d'avoir une bonne documentation est d'avoir un historique pour soi-même, afin de se souvenir de ce qu'on a fait, car on peut facilement oublier ce que l'on a fait. (Si vous avez déjà codé il vous est sans doute déjà arrivé de ne pas comprendre quelque chose que vous avez codé vous-même.)

## Pourquoi utiliser du version control ?

Le version control est un concept utilisé notamment à travers git, qui consiste à garder un historique de tout ce qui a été implémenté dans un projet, permettant de comparer les différentes versions de celui-ci, de revenir à une version antérieure en cas de problème, de travailler différents points d'un projet sur des branches parallèles (utile notamment dans le cas où l'on travaille à plusieurs sur un même projet).

Ceci est extrêmement pratique car cela accélère le travail en parallèle, la correction de problème grâce à la comparaison de version, etc.

## Comment utiliser Git

Ayant déjà utilisé Git dans différents projets, j'en avais déjà une certaine connaissance, j'ai donc essentiellement utilisé les [tutoriel de gitlab](https://docs.gitlab.com/ee/topics/set_up_organization.html) pour me rappeler de son utilisation.

Pour commencer il faut paramétrer son compte gitlab, pour ce faire on utilise les commandes

```
git config --global user.name "nom d'utilisateur"

git config --global user.mail "adresse mail"
```

Pour configurer respectivement le nom d'utilisateur et le mail.

Ensuite, il faut créer un lien entre git et son ordinateur, pour ce faire, j'ai utilisé git gui, qui est une interface de git, et je suis allé dans la barre "Help" et ai cliqué sur "Show SSH key", qui m'a proposé de générer une clé SSH. Ceci peut bien entendu se faire avec des lignes de commandes indiquée sur ce [lien du tutoriel de gitlab](https://docs.gitlab.com/ee/ssh/)

![](https://cdn.discordapp.com/attachments/715249560527831142/946344011373899817/gitguissh.jpg)

Cette clé (qu'il ne faut jamais montré) permet de faire le lien entre un ordinateur et gitlab, via la [partie SSH Key des paramètres utilisateurs de Gitlab](https://gitlab.com/-/profile/keys), en l'entrant dans la zone de texte qui apparait.

Enfin, toujours à l'aide de git gui, j'ai "cloné" mon dossier git via le lien SSH qui est accessible sur le projet sur gitlab, ce qui peut également être fait en utilisant la commande suivante.

```
git clone "SSH Key"
```

Maintenant que le projet est lié au fichier dans lequel la commande précédente est faite, on peut modifier les fichiers du git localement, puis créer de nouvelle branche, commit sur celles-ci (c'est à dire faire l'annonce d'une modification) avant de push (c'est à dire publier les modification sur gitlab), on peut également quand la tâche d'une branche est finie la merge avec une autre branche.

Pour faciliter ces tâches, j'utilise le package git d'[atom](https://atom.io/), qui permet de facilement travailler sur les fichiers md car cette application propose la visualisation du fichier md via un autre package. Voici un résumé des actions principales:
- Checkout (à savoir changer de branche)

![](https://cdn.discordapp.com/attachments/715249560527831142/946361280808243240/unknown.png)

- Commit (prévenir git que l'on a fait une modification)\
Une fois qu'un fichier a été modifié, il apparait dans les "unstaged changes"

![](https://cdn.discordapp.com/attachments/715249560527831142/946363693963288576/gitcommit2.jpg)

Il suffit de le stage (par exemple en appuyant sur stage all), ensuite d'écrire un message de commit et enfin d'appuyer sur Commit to ""[nom de branche]".

![](https://cdn.discordapp.com/attachments/715249560527831142/946363693531271188/gitcommit.jpg)

- Push (publier le commit)

Une fois qu'un commit a été fait, il suffit de push en appuyant sur ce bouton

![](https://cdn.discordapp.com/attachments/715249560527831142/946363693757788180/git_push.jpg)

Dans le cas où je dois pull les fichiers, il suffit d'aller sur git bash dans le dossier du projet et d'utiliser la commande

```
git pull
```

## Syntaxe des fichiers Markdown

Les fichiers Markdown permettent d'écrire en gras, en italique, en surligné, et d'autres, voici une liste non exhaustive des choses possibles à faire :

### Gras, italique, surlignage

Pour les écritures spécifiques, il suffit d'entourer ce qu'il faut écrire de caractère spéciaux, par exemple :

**GRAS** : \*\*Ce message est en gras\*\* donnera **Ce message est en gras**

*ITALIQUE* : \*Ce message est en italique\* donnera *Ce message est en italique*

`SURLIGNAGE` : \`Ce message est surligné\` donnera `Ce message est surligné`

On peut également faire des blocs surligné en écrivant :


````
```
Ce bloc est surligné
```
````

On peut même y inclure un langage de programmation pour coller du code sur le markdown en indiquant le langage souhaité, par exemple :


````
```c
#include <stdio.h>
void main(){
  printf("Hello World!");
}
```
````

écrira ceci :

```c
#include <stdio.h>
void main(){
  printf("Hello World!");
}
```

Information supplémentaire : pour éviter de faire ces syntaxes, on peut simplement rajouter un \\ devant les caractères spéciaux pour les écrire normalement.

## Comment compresser une image

Pour compresser une image, il existe divers manières, j'avais personnellement tendance à utiliser paint et d'enregistrer l'image au format jpeg pour utiliser la compresser qui est plutôt efficace.

Une autre méthode est d'employer le logiciel ImageMagick, téléchargeable [ici](https://imagemagick.org/script/download.php), qui a le grand avantage de permettre la modification d'image via des lignes de commandes. Ce qui permet d'automatiser la compression d'image dans un dossier. A noter que j'utilise la version Windows.

Pour apprendre la liste de commande, on peut se fier à la cheatsheet accessible sur ce [lien](https://devhints.io/imagemagick)

Pour réduire la taille des fichiers, ce qui est important pour ne pas avoir un site trop lourd à charger, on peut simplement commencer par réduire le format, il n'est en effet pas nécessaire d'avoir une image en 1080p pour une site. On peut également faire de la compression en jpeg

Voici une commande qui peut être utilisée pour cette compression :

```
magick mogrify -format jpg -quality 85 -resize 100x100 NOM_IMAGE
```

Cette commande convertit l'image **NOM_IMAGE** en un fichier jpg avec une qualité réduite à 85% avec une taille de 100px x 100px. On voit que cette commande est facile d'utilisation et qu'elle permet de convertir rapidement des fichiers.

## Comment compresser une vidéo

Pour la compression de vidéo, on va utiliser un autre logiciel qui permet de le faire via des lignes de commandes : FFMPEG. Une cheatsheet est accessible sur ce [lien](https://gist.github.com/steven2358/ba153c642fe2bb1e47485962df07c730).

On souhaite réduire la taille des fichiers, pour ce faire, on peut simplement réduire la taille, et convertir au format .mp4 qui permet une grande réduction de la taille des fichiers.

Voici une commande qui peut être utilisée pour ce faire

```
ffmpeg -i input.mp4 -vcodec libx265 -crf 28 output.mp4
```

L'image suivante montre que la taille d'une vidéo convertie via cette ligne de commande a été réduite d'une facteur 5. C'est donc une manière très efficace de réduire beaucoup la taille d'une image.

![](./images/Module1/ReducEx.png)

On peut également ajouter le paramètre -an, qui retire l'audio de la vidéo, chose souvent peu importante pour les démonstrations que l'on a à faire.

Pour optimiser pour une page web, on utilisera généralement l'extension .webm plutôt que .mp4. On peut faire la conversion en utilisant

```
ffmpeg -i input.mp4 output.webm
```

## Intégrer des médias au site

### Intégration image

Pour intégrer des images au site, la syntaxe à utiliser est la suivante :

```
![]([lien image])
```

Le media situé à l'adresse [lien image] sera affichée. En général, on va privilégier les liens référentiels plutôt que les absolus, c'est à dire qu'on utiliser `./` ou encore `../`, servant respectivement à prendre comme référence le dossier actuel ou le dossier parent. A noter que pour les vidéos, cela ne fonctionne pas de base.

### Intégration lien

On peut également intégrer des liens avec une méthode similaire

```
[Texte à écrire](lien)
```

### Intégration vidéo

Pour intégrer des vidéos, il faut utiliser un plugin (appelé "mkdocs-video"). Pour ce faire il faut modifier plusieurs choses :

- La première est le fichier "mkdocs.yml", il faut en effet y ajouter le bloc suivant :

```
plugins:
    - mkdocs-video
```

- La seconde chose à modifier est le script qui se lance pour charger les prérequis à la création du site.

Ceci se fait soit directement dans le fichiers ".gitlab-ci.yml" en ajoutant la ligne :

```
pip.exe install mkdocs-video
```

dans la catégorie "before script"

Soit dans notre cas dans le fichier "requirements.txt", en ajoutant simplement mkdocs-video à une nouvelle ligne. A noter que le fichier requirement.txt sert uniquement à simplifier l'ajout de plugin.

Maintenant que mkdocs-video est prêt à être utilisé, il suffit d'utiliser la syntaxe :

```
![type:video](lien video)
```

Et le tour sera joué.

## License open source

Pour partager ses travaux, il est nécessaire de passer par des licences, afin d'indiquer que le travail est open source. Le site gitlab propose des template de licences open source comme la licence MIT que j'ai décidé d'utiliser.
